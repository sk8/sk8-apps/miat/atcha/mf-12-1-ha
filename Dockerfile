
ARG BASE_IMAGE_NAME=registry.forgemia.inra.fr/record/record
ARG BASE_IMAGE_REF=@sha256:93cb949a4228b4da336490900b523483503a07ab07eb8a3b438b66c7e75919e5

##############
## Build stage
##############
# hadolint ignore=DL3006
FROM ${BASE_IMAGE_NAME}${BASE_IMAGE_REF} as build

USER root

ARG DEBIAN_FRONTEND=noninteractive
ENV MAKEFLAGS "-j$(nproc)" 

RUN apt-get update \
    && apt-get upgrade --no-install-recommends --no-install-suggests -y -V \
    && apt-get install --no-install-recommends --no-install-suggests -y -V \
            git \
    && rm -rf /var/lib/apt/lists/*


USER record

# install vle packages from the public repos
WORKDIR /work/doc
WORKDIR /data

ENV ambhas_repo="https://forgemia.inra.fr/record-open-archives/ambhas_cpp.git"
ENV ambhas_version="688247efa3e11b0397ecbca5a46391b4d98fd8c0"
# hadolint ignore=DL3003
RUN git clone ${ambhas_repo} tmp \
 && git -C tmp checkout ${ambhas_version} \
 && printf '%s:%s\n' "${ambhas_repo}" "$(git -C tmp rev-parse HEAD)" > /data/ambhas_sha.info \
 && cp -f /data/tmp/Ambhas_Cpp/doc/Ambhas_Cpp_pkg_doc.md /work/doc/ \
 && (cd tmp \
 && vle -P Ambhas_Cpp configure build test) \
 && rm -rf tmp

ENV pump_repo="https://forgemia.inra.fr/record-open-archives/pump-potential.git"
ENV pump_version="6861b8c0048ccf019518dd21546bf6fd8aa95d52"
# hadolint ignore=DL3003
RUN git clone ${pump_repo} tmp \
 && git -C tmp checkout ${pump_version} \
 && printf '%s:%s\n' "${pump_repo}" "$(git -C tmp rev-parse HEAD)" > /data/pump_sha.info \
 && cp -f /data/tmp/PumpPotential/doc/PumpPotential_pkg_doc.md /work/doc/ \
 && (cd tmp \
 && vle -P PumpPotential configure build test) \
 && rm -rf tmp

ENV ascrom_repo="https://forgemia.inra.fr/record-open-archives/ascrom.git"
ENV ascrom_version="9d28907e8ebfe96867375709d9ac798a654fcb21"
# hadolint ignore=DL3003
RUN git clone ${ascrom_repo} tmp \
 && git -C tmp checkout ${ascrom_version} \
 && printf '%s:%s\n' "${ascrom_repo}" "$(git -C tmp rev-parse HEAD)" > /data/ascrom_sha.info \
 && cp -f /data/tmp/ASCROM/doc/ASCROM_pkg_doc.md /work/doc/ \
 && cp -rf /data/tmp/ASCROM/doc/figures /work/doc/figures/ \
 && (cd tmp \
 && vle -P ASCROM configure build test) \
 && rm -rf tmp

ENV mf12_repo="https://forgemia.inra.fr/record-open-archives/mf1_2.git"
ENV mf12_version="d02db0877e45690fea128e7e0fc0d0122207ab90"
# hadolint ignore=DL3003
RUN git clone ${mf12_repo} tmp \
 && git -C tmp checkout ${mf12_version} \
 && printf '%s:%s\n' "${mf12_repo}" "$(git -C tmp rev-parse HEAD)" > /data/mf12_sha.info \
 && (cd tmp \
 && vle -P MF1_2 configure build test) \
 && rm -rf tmp


# using existing renv in /work
WORKDIR /work

# use rstudio/posit repo to get binary packages
# https://github.com/opensafely-core/r-docker/issues/75
# https://packagemanager.rstudio.com/client/#/repos/2/overview
RUN R -e "renv::install(packages=c('bslib@0.4.2', \
                                   'chron@2.3-59', \
                                   'devtools@2.4.5', \
                                   'DT@0.27', \
                                   'dygraphs@1.1.1.6', \
                                   'fields@14.1', \
                                   'knitr@1.42', \
                                   'pander@0.6.5', \
                                   'RColorBrewer@1.1-3', \
                                   'readr@2.1.3', \
                                   'rmarkdown@2.20', \
                                   'shiny@1.6.0', \
                                   'shinycssloaders@1.0.0', \
                                   'shinyjs@2.1.0', \
                                   'shinylogs@0.2.1', \
                                   'shinyscreenshot@0.2.0', \
                                   'stringr@1.5.0', \
                                   'timevis@2.1.0', \
                                   'usethis@2.1.6', \
                                   'webshot@0.5.4', \
                                   'xts@0.12.2'), \
                        repos='https://packagemanager.rstudio.com/all/__linux__/focal/latest'); \ 
          renv::snapshot(type='all');"

################
## Release stage
################
# hadolint ignore=DL3006
FROM ${BASE_IMAGE_NAME}${BASE_IMAGE_REF} as release

ARG DEBIAN_FRONTEND=noninteractive
ENV MAKEFLAGS "-j$(nproc)" 

USER root

RUN apt-get update \
    && apt-get upgrade --no-install-recommends --no-install-suggests -y -V \
    && apt-get install --no-install-recommends --no-install-suggests -y -V \
            pandoc \
    && rm -rf /var/lib/apt/lists/*


## Copy the application into image 
## Add here files and directory necessary for the app. 
## global.R ui.R server.R ... 
COPY . /work
RUN chown -R record.record /work

USER record
WORKDIR /home/record/.cache/R-sass
WORKDIR /work

# cache renv
COPY --from=build  /home/record/.cache /home/record/.cache 
COPY --from=build --chmod=777 /data /data
# must be after copy of renv cache (sym links)
COPY --from=build --chmod=777 /work /work



RUN cp -f /work/renv.lock /data/renv.info \
    && dpkg --list > /data/package_list_release.info


# generate doc html page from .md files

RUN Rscript -e 'rmarkdown::pandoc_available();rmarkdown::render_site("/work/doc")' \
&&  mv /work/doc/_site /work/www/doc 

ARG REPO_URL="https://forgemia.inra.fr/sk8/sk8-apps/miat/atcha/mf-12-1-ha"
ARG BUILD_DATE="unknown"
ARG VERSION="unknown"
ARG COMMIT="unknown"
ARG BASE_DIGEST="unknown"

LABEL \
    org.opencontainers.image.created=$BUILD_DATE \
    org.opencontainers.image.authors="record_num@groupes.renater.fr" \
    org.opencontainers.image.url="https://mf-12-1-ha.sk8.inrae.fr/" \
    org.opencontainers.image.documentation="$REPO_URL/-/blob/$COMMIT/Dockerfile" \
    org.opencontainers.image.source="$REPO_URL" \
    org.opencontainers.image.version="$VERSION" \
    org.opencontainers.image.revision=$COMMIT \
    org.opencontainers.image.vendor="Record Team - INRAE" \
    org.opencontainers.image.licenses="GPL-3.0" \
    org.opencontainers.image.title="sk8-mf12" \
    org.opencontainers.image.description="Shiny app for sk8" \
    org.opencontainers.image.base.digest="$BASE_DIGEST" \
    org.opencontainers.image.base.name="${BASE_IMAGE_NAME}"

EXPOSE 3838 

CMD ["sh", "-c", "Rscript -e \"shiny::runApp('/work', port = 3838, host = '0.0.0.0' )\""]
