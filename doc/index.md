---
title: "Reference documents for the MF1_2 Shiny App"
---

# Shiny App  
* [MF1_2 indicators description](./Shiny-indicators.html)   
* [MF1_2 interface input file description](./Shiny-data-files.html)   
* [sequence to kc pairs conversion algorithm](./Shiny-sequence-to-kcpairs.html)   
* [kc climatic correction algorithm](./Shiny-kc-ClimaticCorrection.html)   

***

# VLE packages documentation
* [ASCROM (Crop model)](./ASCROM_pkg_doc.html)  
* [Ambhas (Groundwater model)](./Ambhas_Cpp_pkg_doc.html)  
* [PumpPotential (Pump flow model)](./PumpPotential_pkg_doc.html)  
